## Usage

> *Pay attention, docker and docker-compose must be installed to use commands below.*
>
> *All commands should be run in UNIX-like terminal.*

### Install project, full list of command you can find in the Makefile
```shell
$ git clone [repo] moohii.tt
$ cd moohii.tt
$ make init
```

Add next line to your hosts file 
> 127.0.0.1 moohii.tt 

Now you can use api via [moohii.tt/api](http://moohii.tt/api) URL

To start docker containers
```shell
$ make start
```

To login into php container
```shell
$ make php
```

To stop docker containers
```shell
$ make stop
```
