<?php

namespace App\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Article
 *
 * @package App\Eloquent\Models
 */
class Article extends Model
{
    use SoftDeletes;

    #region Constants

    /**
     * Key for determine model in polymorphic relations
     */
    public const MORPH_KEY = 'article';

    #endregion

    #region Properties

    protected $casts = [
        'created_at' => 'timestamp',
    ];

    protected $visible = [
        'content',
        'created_at',
        'id',
        'title',
    ];

    #endregion

    #region Methods

    public function isAuthor(User $user) : bool
    {
        return  $this->getUserId() === $user->getId();
    }

    #endregion

    #region Relations

    /**
     * @return MorphMany
     */
    public function comments() : MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    #endregion

    #region Getters

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->getAttribute('user_id');
    }

    #endregion

    #region Setters

    /**
     * @param string $value
     */
    public function setContent(string $value) : void
    {
        $this->setAttribute('content', $value);
    }

    /**
     * @param string $value
     */
    public function setTitle(string $value) : void
    {
        $this->setAttribute('title', $value);
    }

    #endregion
}
