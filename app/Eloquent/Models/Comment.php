<?php

namespace App\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    #region Relations

    /**
     * @return MorphTo
     */
    public function commentable() : MorphTo
    {
        return $this->morphTo();
    }

    #endregion

    #region Methods

    public function setAuthor(User $user)
    {
        $this->setUserId($user->getId());
    }

    #region Setters

    /**
     * @param string $value
     */
    public function setText(string $value) : void
    {
        $this->setAttribute('text', $value);
    }

    /**
     * @param int $value
     */
    public function setUserId(int $value) : void
    {
        $this->setAttribute('user_id', $value);
    }

    #endregion
}
