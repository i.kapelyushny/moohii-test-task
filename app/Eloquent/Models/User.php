<?php

namespace App\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * Class User
 *
 * @package App\Eloquent\Models
 *
 * @method Builder|User whereEmail(string $value) See scopeWhereEmail
 */
class User extends Authenticatable
{
    use Notifiable;

    #region Constants

    const API_TOKEN_LENGTH = 50;

    #endregion

    #region Properties

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'api_token',
        'password',
        'remember_token',
    ];

    #endregion

    #region Relations

    /**
     * @return HasMany
     */
    public function articles() : HasMany
    {
        return $this->hasMany(Article::class);
    }

    #endregion

    #region Methods

    public function generateApiToken()
    {
        $this->setApiToken(Str::random(self::API_TOKEN_LENGTH));
    }

    /**
     * @return string
     */
    public function getApiTokenColumn() : string
    {
        return 'api_token';
    }

    /**
     * @return string
     */
    public function getEmailColumn() : string
    {
        return 'email';
    }

    /**
     * @return string
     */
    public function getNameColumn() : string
    {
        return 'name';
    }

    /**
     * @return string
     */
    public function getPasswordColumn() : string
    {
        return 'password';
    }

    /**
     * @param string $passwordToCheck
     *
     * @return bool
     */
    public function isValidPassword(string $passwordToCheck) : bool
    {
        return Hash::check($passwordToCheck, $this->getPassword());
    }

    #endregion

    #region Getters

    /**
     * @return string
     */
    public function getApiToken() : string
    {
        return $this->getAttribute($this->getApiTokenColumn());
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute('id');
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->getAttribute($this->getPasswordColumn());
    }

    #endregion

    #region Setters

    /**
     * @param string $value
     */
    public function setApiToken(string $value) : void
    {
        $this->setAttribute($this->getApiTokenColumn(), $value);
    }

    /**
     * @param string $value
     */
    public function setEmail(string $value) : void
    {
        $this->setAttribute($this->getEmailColumn(), $value);
    }

    /**
     * @param string $value
     */
    public function setName(string $value) : void
    {
        $this->setAttribute($this->getNameColumn(), $value);
    }

    /**
     * @param string $value
     */
    public function setPassword(string $value) : void
    {
        $this->setAttribute(
            $this->getPasswordColumn(),
            Hash::make($value)
        );
    }

    #endregion

    #region Scopes

    /**
     * @param Builder $builder
     * @param string  $value
     *
     * @return Builder
     */
    public function scopeWhereEmail(Builder $builder, string $value) : Builder
    {
        return $builder->where($this->getEmailColumn(), '=', $value);
    }

    #endregion
}
