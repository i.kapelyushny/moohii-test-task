<?php

namespace App\Eloquent\Observers;

use App\Eloquent\Models\User;

class UserObserver
{
    /**
     * Handle the user "creating" event.
     *
     * @param  User  $user
     * @return void
     */
    public function creating(User $user) : void
    {
        $user->generateApiToken();
    }
}
