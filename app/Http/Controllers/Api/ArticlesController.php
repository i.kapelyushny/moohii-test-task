<?php

namespace App\Http\Controllers\Api;

use App\Eloquent\Models\Article;
use App\Eloquent\Models\Comment;
use App\Eloquent\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Articles\AddCommentRequest;
use App\Http\Requests\Api\Articles\CreateArticleRequest;
use App\Http\Requests\Api\Articles\DeleteArticleRequest;
use App\Http\Requests\Api\Articles\UpdateArticleRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\Api
 */
class ArticlesController extends Controller
{
    /**
     * @param AddCommentRequest $addCommentRequest
     * @param Article           $article
     *
     * @return HttpResponse
     */
    public function addComment(AddCommentRequest $addCommentRequest, Article $article) : HttpResponse
    {
        /** @var User $user */
        $user = Auth::user();
        $comment = new Comment();
        $comment->setAuthor($user);
        $comment->setText($addCommentRequest->getText());
        $article->comments()->save($comment);

        return Response::noContent();
    }

    /**
     * @param CreateArticleRequest $createArticleRequest
     *
     * @return HttpResponse
     */
    public function createArticle(CreateArticleRequest $createArticleRequest) : HttpResponse
    {
        /** @var User $user */
        $user = Auth::user();

        $article = new Article();
        $article->setContent($createArticleRequest->getArticleContent());
        $article->setTitle($createArticleRequest->getTitle());

        $user->articles()->save($article);

        return Response::noContent();
    }

    /**
     * @return JsonResponse
     */
    public function getArticles() : JsonResponse
    {
        return Response::json(
            Article::all()
        );
    }

    /**
     * @param Article $article
     *
     * @return JsonResponse
     */
    public function deleteArticle(DeleteArticleRequest $request, Article $article) : HttpResponse
    {
        $article->delete();

        return Response::noContent();
    }

    /**
     * @param Article $article
     *
     * @return JsonResponse
     */
    public function getArticle(Article $article) : JsonResponse
    {
        return Response::json($article);
    }

    /**
     * @param UpdateArticleRequest $updateArticleRequest
     * @param Article              $article
     *
     * @return HttpResponse
     */
    public function updateArticle(UpdateArticleRequest $updateArticleRequest, Article $article) : HttpResponse
    {
        $article->setContent($updateArticleRequest->getArticleContent());
        $article->setTitle($updateArticleRequest->getTitle());
        $article->save();

        return Response::noContent();
    }
}
