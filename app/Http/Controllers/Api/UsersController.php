<?php

namespace App\Http\Controllers\Api;

use App\Eloquent\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\AuthenticationRequest;
use App\Http\Requests\Api\Users\RegistrationRequest;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as HttpResponse;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\Api
 */
class UsersController extends Controller
{
    /**
     * @param RegistrationRequest $request
     *
     * @return HttpResponse
     */
    public function register(RegistrationRequest $request) : HttpResponse
    {
        $user = new User();
        $user->setEmail($request->getEmail());
        $user->setName($request->getName());
        $user->setPassword($request->getPassword());
        $user->save();

        return Response::noContent();
    }

    /**
     * @param AuthenticationRequest $request
     *
     * @return JsonResponse
     *
     * @throws AuthenticationException
     */
    public function authenticate(AuthenticationRequest $request) : JsonResponse
    {
        /** @var User|null $user */
        $user = User::whereEmail($request->getEmail())->first();

        if ($user === null || !$user->isValidPassword($request->getPassword())) {
            throw new AuthenticationException();
        }

        return Response::json([
            'api_token' => $user->getApiToken(),
        ]);
    }
}
