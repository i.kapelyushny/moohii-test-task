<?php

namespace App\Http\Requests\Api\Articles;

use App\Eloquent\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AddCommentToArticleRequest
 *
 * @package App\Http\Requests\Api\Articles
 */
class AddCommentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => [
                'required',
                'string',
                'max:255',
            ]
        ];
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->get('text');
    }
}
