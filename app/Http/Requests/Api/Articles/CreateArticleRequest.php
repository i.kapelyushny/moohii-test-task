<?php

namespace App\Http\Requests\Api\Articles;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateArticleRequest
 *
 * @package App\Http\Requests\Api\Users
 */
class CreateArticleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => [
                'required',
                'string',
                'max:255',
            ],
            'content' => [
                'required',
                'string',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->get('title');
    }

    /**
     * @return string
     */
    public function getArticleContent() : string
    {
        return $this->get('content');
    }
}
