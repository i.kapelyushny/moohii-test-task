<?php

namespace App\Http\Requests\Api\Articles;

use App\Eloquent\Models\Article;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DeleteArticleRequest
 *
 * @package App\Http\Requests\Api\Articles
 */
class DeleteArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var Article $article */
        $article = $this->route('article');

        return $article !== null && $article->isAuthor($this->user());
    }

    public function rules()
    {
        return [];
    }
}
