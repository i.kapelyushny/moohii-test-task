<?php

namespace App\Http\Requests\Api\Articles;

use App\Eloquent\Models\Article;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateArticleRequest
 *
 * @package App\Http\Requests\Api\Articles
 */
class UpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var Article $article */
        $article = $this->route('article');

        return $article !== null && $article->isAuthor($this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => [
                'required',
                'string',
                'max:255',
            ],
            'content' => [
                'required',
                'string',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->get('title');
    }

    /**
     * @return string
     */
    public function getArticleContent() : string
    {
        return $this->get('content');
    }
}
