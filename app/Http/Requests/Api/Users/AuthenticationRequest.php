<?php

namespace App\Http\Requests\Api\Users;

use App\Eloquent\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AuthenticationRequest
 *
 * @package App\Http\Requests\Api\Users
 */
class AuthenticationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
            ],
            'password' => [
                'required',
                'string',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->get('email');
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->get('password');
    }
}
