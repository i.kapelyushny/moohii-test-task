<?php

namespace App\Http\Requests\Api\Users;

use App\Eloquent\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class RegistrationRequest
 *
 * @package App\Http\Requests\Api\Users
 */
class RegistrationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $usersModel = new User();

        return [
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique(
                    $usersModel->getTable(),
                    $usersModel->getEmailColumn()
                ),
            ],
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'password' => [
                'required',
                'string',
                'min:8',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->get('email');
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->get('name');
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->get('password');
    }
}
