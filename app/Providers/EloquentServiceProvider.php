<?php

namespace App\Providers;

use App\Eloquent\Models\Article;
use App\Eloquent\Models\User;
use App\Eloquent\Observers\UserObserver;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class EloquentServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->definePolymorphicRelationsMap();
        $this->registerObservers();

    }

    /**
     * Определение псевдонимов моделей в полиморфных связях
     */
    private function definePolymorphicRelationsMap() : void
    {
        Relation::morphMap([
            Article::MORPH_KEY => Article::class,
        ]);
    }

    /**
     * Регистраиция слушателей событий моделей
     */
    private function registerObservers()
    {
        User::observe(UserObserver::class);
    }
}
