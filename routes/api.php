<?php

use Illuminate\Support\Facades\Route;

Route::post('register', 'Api\UsersController@register');
Route::post('authenticate', 'Api\UsersController@authenticate');

Route::prefix('articles')->group(function () {
    Route::get('/', 'Api\ArticlesController@getArticles');
    Route::get('{article}', 'Api\ArticlesController@getArticle');

    Route::middleware('auth:api')->group(function () {
        Route::post('/', 'Api\ArticlesController@createArticle');
        Route::prefix('{article}')->group(function () {
            Route::patch('/', 'Api\ArticlesController@updateArticle');
            Route::delete('/', 'Api\ArticlesController@deleteArticle');
            Route::post('comments', 'Api\ArticlesController@addComment');
        });
    });
});

